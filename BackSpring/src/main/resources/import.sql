INSERT INTO Region (nombre_Region) VALUES ('Sudamérica');
INSERT INTO Region (nombre_Region) VALUES ('Centroamerica');
INSERT INTO Region (nombre_Region) VALUES ('Norteamérica');
INSERT INTO Region (nombre_Region) VALUES ('Europa');
INSERT INTO Region (nombre_Region) VALUES ('Asia');
INSERT INTO Region (nombre_Region) VALUES ('Africa');
INSERT INTO Region (nombre_Region) VALUES ('Oceanía');
INSERT INTO Region (nombre_Region) VALUES ('Antartida');

INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Svein', 'Navarro', '1@mail.com' , '2022/06/02', 3);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Diana', 'Rivera', '2@mail.com' , '2022/06/02', 2);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Alejandra', 'Ruiz', '3@mail.com' , '2022/06/02', 1);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Carlos', 'Segura', '4@mail.com' , '2022/06/02', 2);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Rodrigo', 'Diaz', '5@mail.com' , '2022/06/02', 3);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Alejandro', 'Torres', '6@mail.com' , '2022/06/02', 4);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Raul', 'Robles', '7@mail.com' , '2022/06/02', 5);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Selene', 'Lara', '8@mail.com' , '2022/06/02', 6);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Luxanna', 'Crawngauard', '9@mail.com' , '2022/06/02', 7);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Katarina', 'DoCoteau', '10@mail.com' , '2022/06/02', 8);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Garen', 'Crawnguard', '11@mail.com' , '2022/06/02', 8);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Jaime', 'Duende', '12@mail.com' , '2022/06/02', 7);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('James', 'Gosling', '13@mail.com' , '2022/06/02', 6);
INSERT INTO Cliente (nombre, apellido, correo, fecha, region_Id) values ('Linus', 'Torvalds', '14@mail.com' , '2022/06/02', 5);

INSERT INTO usuario (usuario, contrasena, habilitado, nombre, apellido, correo) values ('admin', '$2a$10$QYThCAkBNpR7CPbFNNkIR.dvciFdELiP2lqDvtmcqSqclZ3hRLzHy', 1, 'Svein', 'Navarro', 'cnavarros1400@mail.com');
INSERT INTO usuario (usuario, contrasena, habilitado, nombre, apellido, correo) values ('admin-user', '$2a$10$UcM6cl.kbvKbdkWLLuSTruiz28aMX/H5V9zVZ89qzltuGEw1EVlna', 1, 'Diana', 'Rivera', 'rive@mail.com');
INSERT INTO usuario (usuario, contrasena, habilitado, nombre, apellido, correo) values ('user', '$2a$10$liEk3OPt2q/o2cluaFQQlOhn4e8pBRkL74wbZkt.4voyhixtIdgRi', 1, 'Gustavo', 'Rojas', 'gus_rojas@mail.com');

INSERT INTO rol (nombrerol) values ('ROLE_ADMIN');
INSERT INTO rol (nombrerol) values ('ROLE_USER');

INSERT INTO usuario_roles (usuario_idusuario, roles_idrol) values (1, 1);
INSERT INTO usuario_roles (usuario_idusuario, roles_idrol) values (2, 1);
INSERT INTO usuario_roles (usuario_idusuario, roles_idrol) values (2, 2);
INSERT INTO usuario_roles (usuario_idusuario, roles_idrol) values (3, 2);

INSERT INTO producto (nombre, precio, fecha_registro) VALUES('Panasonic Pantalla LCD', 259990, NOW());
INSERT INTO producto (nombre, precio, fecha_registro) VALUES('Sony Camara digital DSC-W320B', 123490, NOW());
INSERT INTO producto (nombre, precio, fecha_registro) VALUES('Apple iPod shuffle', 1499990, NOW());
INSERT INTO producto (nombre, precio, fecha_registro) VALUES('Sony Notebook Z110', 37990, NOW());
INSERT INTO producto (nombre, precio, fecha_registro) VALUES('Hewlett Packard Multifuncional F2280', 69990, NOW());
INSERT INTO producto (nombre, precio, fecha_registro) VALUES('Bianchi Bicicleta Aro 26', 69990, NOW());
INSERT INTO producto (nombre, precio, fecha_registro) VALUES('Mica Comoda 5 Cajones', 299990, NOW());

INSERT INTO factura (descripcion, observacion, cliente_id, fecha_registro) VALUES('Factura equipos de oficina', null, 1, NOW());
INSERT INTO item_factura (cantidad, factura_id, producto_id) VALUES(1, 1, 1);
INSERT INTO item_factura (cantidad, factura_id, producto_id) VALUES(2, 1, 4);
INSERT INTO item_factura (cantidad, factura_id, producto_id) VALUES(1, 1, 5);
INSERT INTO item_factura (cantidad, factura_id, producto_id) VALUES(1, 1, 7);

INSERT INTO factura (descripcion, observacion, cliente_id, fecha_registro) VALUES('Factura Bicicleta', 'Alguna nota importante!', 1, NOW());
INSERT INTO item_factura (cantidad, factura_id, producto_id) VALUES(3, 2, 6);