package com.bolsaideas.controlador;

import com.bolsaideas.modelo.entity.Cliente;
import com.bolsaideas.modelo.entity.Region;
import com.bolsaideas.modelo.servicio.ClienteServicio;
import com.bolsaideas.modelo.servicio.SubirImagenServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import java.util.stream.Collectors;


@CrossOrigin(origins = {"http://localhost:4200", "*"})
@RestController
@RequestMapping("/apiCliente")
public class ClienteControlador {

    @Autowired
    private ClienteServicio clienteServicio;

    @Autowired
    private SubirImagenServicio subirImagenServicio;

    @GetMapping("/listado")
    @ResponseStatus(HttpStatus.OK)
    public List<Cliente> listadoClientes() {
        return clienteServicio.findAll();
    }

    @GetMapping("/listado/pagina/{pagina}")
    @ResponseStatus(HttpStatus.OK)
    public Page<Cliente> listadoClientes(@PathVariable Integer pagina) {
        Pageable pageable = PageRequest.of(pagina, 5);
        return clienteServicio.findAll(pageable);
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping("/cliente/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> buscarPorId(@PathVariable Long id) {
        Cliente cliente;
        Map<String, Object> respuesta = new HashMap<>();
        try {
            cliente = clienteServicio.findById(id);
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Error al accesar a fuente de datos");
            respuesta.put("Error", Objects.requireNonNull(e.getMessage()).concat(" : ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (cliente == null) {
            respuesta.put("mensaje", "Cliente con id ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<>(respuesta, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cliente, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/cliente")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?>crearCliente(@Valid @RequestBody Cliente cliente, BindingResult result) {
        Cliente nuevoCliente;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()){
            //Codigo en java8
//            List<String> errores = new ArrayList<>();
//            for(FieldError e : result.getFieldErrors()){
//                errores.add("Error en el campo '" + e.getField() + "'. " + e.getDefaultMessage());
//            }
            //Codigo version > java 8 usando Streams
            List<String> errores = result.getFieldErrors()
                                    .stream()
                                    .map(e -> "Error en el campo '" + e.getField() + "'. " + e.getDefaultMessage())
                                    .collect(Collectors.toList());
            response.put("errores", errores);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            nuevoCliente = clienteServicio.save(cliente);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al escribir datos");
            response.put("Error", Objects.requireNonNull(e.getMessage()).concat(" : ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("Registro creado", "Cliente creado exitosamente");
        response.put("Cliente", nuevoCliente);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/cliente/{id}")
    public ResponseEntity<?> modificar(@Valid @RequestBody Cliente cliente, BindingResult result, @PathVariable Long id) {
        Cliente clienteModificar = clienteServicio.findById(id);
        Cliente modificado;
        Map<String, Object> response = new HashMap<>();

        if(result.hasErrors()){
            List<String> errores = result.getFieldErrors().stream().map(e -> "Error en el campo '" + e.getField() + "'. " + e.getDefaultMessage()).collect(Collectors.toList());
            response.put("errores", errores);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        if (clienteModificar == null) {
            response.put("mensaje", "Cliente con id ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        try {
            clienteModificar.setNombre(cliente.getNombre());
            clienteModificar.setApellido(cliente.getApellido());
            clienteModificar.setCorreo(cliente.getCorreo());
            clienteModificar.setRegion(cliente.getRegion());
            modificado = clienteServicio.save(clienteModificar);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al escribir en base de datos");
            response.put("Error", Objects.requireNonNull(e.getMessage()).concat(" : ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("Mensaje", "Datos modificados correctamente");
        response.put("modificacion", modificado);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/cliente/{id}")
    public ResponseEntity<?> eliminarCliente(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        try{
            Cliente cliente = this.clienteServicio.findById(id);
            subirImagenServicio.eliminar(cliente.getImagen());
            clienteServicio.delete(id);
        }catch(DataAccessException e){
            response.put("mensaje", "Error al leer fuente de datos");
            response.put("Error", Objects.requireNonNull(e.getMessage()).concat(" : ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("Mensaje", "Registro de cliente eliminado exitosamente");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PostMapping("/cliente/imagen-perfil")
    public ResponseEntity<?> subirImagen(@RequestParam("archivo")MultipartFile archivo, @RequestParam("id") Long id){
        Map<String, Object> response = new HashMap<>();
        Cliente cliente = this.clienteServicio.findById(id);
        String nombreArchivo = null;
        if (!archivo.isEmpty()) {
            try{
                nombreArchivo = subirImagenServicio.copiar(archivo);
            }catch (IOException e){
                response.put("mensaje", "Error al cargar imagen de perfil");
                response.put("Error", e.getMessage().concat(" : ").concat(e.getCause().getMessage()));
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        subirImagenServicio.eliminar(cliente.getImagen());
        cliente.setImagen(nombreArchivo);
        this.clienteServicio.save(cliente);
        response.put("cliente", cliente);
        response.put("mensaje", "Se ha cargado imagen de perfil correctamente");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/uploads/img/{nombreImagen:.+}")
    public ResponseEntity<Resource> verImagen(@PathVariable String nombreImagen){

        Resource recurso = null;

        try {
            recurso = subirImagenServicio.cargar(nombreImagen);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpHeaders cabeceras = new HttpHeaders();
        cabeceras.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + Objects.requireNonNull(recurso).getFilename() + "\"");
        return new ResponseEntity<>(recurso, cabeceras, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/regiones")
    public List<Region> listarRegiones(){
        return this.clienteServicio.findAllRegiones();
    }

}
