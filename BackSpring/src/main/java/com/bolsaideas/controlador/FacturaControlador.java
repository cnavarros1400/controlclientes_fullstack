package com.bolsaideas.controlador;

import com.bolsaideas.modelo.entity.Factura;
import com.bolsaideas.modelo.entity.Producto;
import com.bolsaideas.modelo.servicio.ClienteServicio;
import com.bolsaideas.modelo.servicio.FacturaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:4200", "*"})
@RequestMapping("/apiFactura")
public class FacturaControlador {
    @Autowired
    private FacturaServicio facturaServicio;

    @Autowired
    private ClienteServicio clienteServicio;

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping("/factura/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public Factura mostrarFactura(@PathVariable Long id){
        return facturaServicio.findByIdFactura(id);
    }

    @Secured({"ROLE_ADMIN"})
    @DeleteMapping("/factura/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delte(@PathVariable Long id){
        facturaServicio.deleteFactura(id);
    }

    @Secured({"ROLE_ADMIN"})
    @GetMapping("/factura/buscar-producto/{termino}")
    @ResponseStatus(HttpStatus.OK)
    public List<Producto> filtarProducto(@PathVariable String termino){
        return clienteServicio.findProductoByNombre(termino);
    }

    @Secured({"ROLE_ADMIN"})
    @PostMapping("/factura")
    @ResponseStatus(HttpStatus.CREATED)
    public Factura crearFactura(@RequestBody Factura factura){
        return facturaServicio.saveFactura(factura);
    }

}
