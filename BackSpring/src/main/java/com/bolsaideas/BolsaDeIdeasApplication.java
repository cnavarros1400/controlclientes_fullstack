package com.bolsaideas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class BolsaDeIdeasApplication implements CommandLineRunner {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(BolsaDeIdeasApplication.class, args);
    }

    //ejecuta algo antes de arrancar app
    @Override
    public void run(String... args) throws Exception {
        String pass = "12345678910";

        for (int i = 0; i < 4; i++) {
            String passEncriptado = bCryptPasswordEncoder.encode(pass);
            System.out.println("Contraseña " + (i + 1) + ": " + passEncriptado);
        }
    }
}
