package com.bolsaideas.modelo.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Region implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombreRegion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegion() {
        return nombreRegion;
    }

    public void setRegion(String region) {
        this.nombreRegion = region;
    }
}
