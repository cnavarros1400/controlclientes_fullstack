package com.bolsaideas.modelo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Cliente implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 255)
    @NotEmpty(message = "Campo nombre no puede estar vacio")
    @Size(min = 5, max = 100, message = "Campo nombre debe contener almenos 5 caracteres")
    private String nombre;
    @Column(nullable = false, length = 255)
    @NotEmpty(message = "Campo apellido no puede estar vacio")
    @Size(min = 5, max = 100, message = "Campo apellido debe contener almenos 5 caracteres")
    private String apellido;
    @Column(nullable = false, length = 255, unique = false)
    @NotEmpty(message = "Campo correo no puede estar vacio")
    @Size(min = 5, max = 100, message = "Campo correo debe contener almenos 5 caracteres")
    @Email(message = "Favor de ingresar un formato de correo válido")
    private String correo;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    @NotNull(message = "Campo fecha no puede estar vacio")
    private Date fecha;
    private String imagen;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "regionId")
    @NotNull(message = "Campo Region es obligatorio")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Region region;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", cascade = CascadeType.ALL)//"cliente" campo en la columna referenciada para bidireccionalidad
    @JsonIgnoreProperties(value = {"cliente", "hibernateLazyInitializer", "handler"}, allowSetters = true)//ignora campo de la tabla referenciada
    private List<Factura> facturas;


    public Cliente(){
        this.facturas = new ArrayList<>();
    }

    public Cliente(String nombre, String apellido, String correo, Date fecha) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.fecha = fecha;
    }

    public Cliente(Long id, String nombre, String apellido, String correo, Date fecha) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.fecha = fecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }
}
