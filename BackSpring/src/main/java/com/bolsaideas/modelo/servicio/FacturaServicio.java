package com.bolsaideas.modelo.servicio;

import com.bolsaideas.modelo.entity.Factura;

public interface FacturaServicio {

    public Factura findByIdFactura(Long id);
    public Factura saveFactura(Factura factura);
    public void deleteFactura(Long id);

}
