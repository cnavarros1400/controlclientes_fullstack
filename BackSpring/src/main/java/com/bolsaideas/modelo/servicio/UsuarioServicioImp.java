package com.bolsaideas.modelo.servicio;

import com.bolsaideas.modelo.dao.UsuarioDao;
import com.bolsaideas.modelo.entity.Usuario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

//UserDetailsService interface natica de spring security
@Service
public class UsuarioServicioImp implements UserDetailsService, UsuarioService {

    private Logger log = LoggerFactory.getLogger(UsuarioServicioImp.class);

    @Autowired
    private UsuarioDao usuarioDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioDao.findByUsuario(username);
        if (usuario == null){
            log.error("Error de acceso, usuario '" + username + "' no existe en el sistema");
            throw new UsernameNotFoundException("Error de acceso, usuario '" + username + "' no existe en el sistema");
        }
        List<GrantedAuthority> autorizaciones = usuario.getRoles()
                .stream()
                .map(rol -> {
                    return new SimpleGrantedAuthority(rol.getNombrerol());
                })
                .peek(autoridad -> log.info("Rol: " + autoridad.getAuthority()))
                .collect(Collectors.toList());
        return new User(usuario.getUsuario(), usuario.getContrasena(), usuario.isHabilitado(), true, true, true, autorizaciones);
    }

    @Override
    @Transactional(readOnly = true)
    public Usuario findByUsername(String username) {
        return usuarioDao.findByUsuario(username);
    }
}
