package com.bolsaideas.modelo.servicio;

import com.bolsaideas.modelo.dao.ClienteDao;
import com.bolsaideas.modelo.dao.FacturaDao;
import com.bolsaideas.modelo.dao.ProductoDao;
import com.bolsaideas.modelo.entity.Cliente;
import com.bolsaideas.modelo.entity.Factura;
import com.bolsaideas.modelo.entity.Producto;
import com.bolsaideas.modelo.entity.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteServicioImp implements ClienteServicio, FacturaServicio{

    @Autowired
    private ClienteDao clienteDao;

    @Autowired
    private FacturaDao facturaDao;

    @Autowired
    private ProductoDao productoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Cliente> findAll() {
        return (List<Cliente>) clienteDao.findAll();
    }

    @Override
    @Transactional
    public Page<Cliente> findAll(Pageable pageable) {
        return clienteDao.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Cliente findById(Long idCliente) {
        return clienteDao.findById(idCliente).orElse(null);
    }

    @Override
    @Transactional
    public Cliente save(Cliente cliente) {
        return clienteDao.save(cliente);
    }

    @Override
    @Transactional
    public void delete(Long idCliente) {
        clienteDao.deleteById(idCliente);
    }

    @Override
    public List<Region> findAllRegiones() {
        return clienteDao.findAllRegiones();
    }

    @Override
    @Transactional
    public List<Producto> findProductoByNombre(String termino) {
        return productoDao.findByNombreContainingIgnoreCase(termino);
    }

    @Override
    @Transactional(readOnly = true)
    public Factura findByIdFactura(Long id) {
        return facturaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Factura saveFactura(Factura factura) {
        return facturaDao.save(factura);
    }

    @Override
    @Transactional
    public void deleteFactura(Long id) {
        facturaDao.deleteById(id);
    }

}
