package com.bolsaideas.modelo.servicio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

@Service
public class SubirImagenServicioImp implements SubirImagenServicio {

    private final Logger log = LoggerFactory.getLogger(SubirImagenServicioImp.class);
    private final static String DIRECTORIO_UPLOAD = "uploads";

    @Override
    public Resource cargar(String nombreImagen) throws MalformedURLException {
        Path rutaArchivo = getPath(nombreImagen);
        log.info("Log. " + rutaArchivo.toString());
        Resource recurso = new UrlResource(rutaArchivo.toUri());
        if (!recurso.exists() && !recurso.isReadable()) {
            rutaArchivo = Paths.get("src/main/resources/static/imagenes").resolve("blank-user-img.jpg").toAbsolutePath();
            recurso = new UrlResource(rutaArchivo.toUri());
            log.error("Error al acceder al recurso '" + nombreImagen + "'");
        }
        return recurso;
    }

    @Override
    public String copiar(MultipartFile archivo) throws IOException {
        String nombreArchivo = UUID.randomUUID().toString() + "_" + Objects.requireNonNull(archivo.getOriginalFilename()).replace(" ", "-");
        Path rutaArchivo = getPath(nombreArchivo);
        log.info("Log. " + rutaArchivo.toString());
        Files.copy(archivo.getInputStream(), rutaArchivo);
        return nombreArchivo;
    }

    @Override
    public boolean eliminar(String nombreImg) {
        if (nombreImg != null && nombreImg.length() > 0) {
            Path rutaImgAnterior = Paths.get("uploads").resolve(nombreImg).toAbsolutePath();
            File archivoAnterior = rutaImgAnterior.toFile();
            if (archivoAnterior.exists() && archivoAnterior.canRead()) {
                archivoAnterior.delete();
                return true;
            }
        }
        return false;
    }

    @Override
    public Path getPath(String nombreImg) {
        return Paths.get(DIRECTORIO_UPLOAD).resolve(nombreImg).toAbsolutePath();
    }
}
