package com.bolsaideas.modelo.servicio;

import com.bolsaideas.modelo.entity.Usuario;

public interface UsuarioService {

    public Usuario findByUsername(String username);

}
