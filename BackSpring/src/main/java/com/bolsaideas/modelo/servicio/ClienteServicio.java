package com.bolsaideas.modelo.servicio;

import com.bolsaideas.modelo.entity.Cliente;
import com.bolsaideas.modelo.entity.Producto;
import com.bolsaideas.modelo.entity.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClienteServicio{

    public List<Cliente> findAll();
    public Page<Cliente> findAll(Pageable pageable);
    public Cliente findById(Long idCliente);
    public Cliente save(Cliente cliente);
    public void delete(Long idCliente);
    public List<Region> findAllRegiones();
    public List<Producto> findProductoByNombre(String termino);

}
