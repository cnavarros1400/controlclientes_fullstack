package com.bolsaideas.modelo.servicio;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

public interface SubirImagenServicio {
    public Resource cargar(String nombreImagen) throws MalformedURLException;
    public String copiar(MultipartFile archivo) throws IOException;
    public boolean eliminar(String nombreImg);
    public Path getPath(String nombreImg);
}
