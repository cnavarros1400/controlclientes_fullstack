package com.bolsaideas.modelo.dao;

import com.bolsaideas.modelo.entity.Producto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductoDao extends CrudRepository<Producto, Long> {

        List<Producto> findByNombreContainingIgnoreCase(String termino);
    List<Producto> findByNombreStartingWithIgnoreCase(String termino);
}
