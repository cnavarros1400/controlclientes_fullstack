package com.bolsaideas.modelo.dao;

import com.bolsaideas.modelo.entity.Cliente;
import com.bolsaideas.modelo.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
//import org.springframework.data.repository.CrudRepository;

public interface ClienteDao extends JpaRepository<Cliente, Long> {
    @Query("from Region")
    public List<Region> findAllRegiones();
}
