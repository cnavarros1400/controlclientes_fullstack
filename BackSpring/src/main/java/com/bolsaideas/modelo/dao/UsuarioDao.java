package com.bolsaideas.modelo.dao;

import com.bolsaideas.modelo.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioDao extends CrudRepository<Usuario, Long> {
    public Usuario findByUsuario(String usuario);//consulta mediante nombre de metodo (implementa "tras bambalinas" consulta jpql
    @Query("select u from Usuario u where u.usuario=?1")
    public Usuario findByUsuarioJPQL(String usuario);//consulta directa sobre jpql
}
