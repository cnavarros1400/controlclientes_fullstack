package com.bolsaideas.modelo.dao;

import com.bolsaideas.modelo.entity.Factura;
import org.springframework.data.repository.CrudRepository;

public interface FacturaDao extends CrudRepository<Factura, Long> {
}
