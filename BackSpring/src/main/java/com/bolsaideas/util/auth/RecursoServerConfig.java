package com.bolsaideas.util.auth;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableResourceServer
public class RecursoServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //prefijo ROLE_ es agreagdo automaticamente a los roles por el metodo .hasRole y .hasAnyRole
        http.authorizeRequests().antMatchers(HttpMethod.GET,"/apiCliente/listado", "/apiCliente/listado/pagina/**", "/apiCliente/uploads/img/**", "/imagenes/**").permitAll()
                /*antMatchers reemplazados por anotacion @Secure en controlador
                .antMatchers(HttpMethod.GET,"/apiCliente/cliente/{id}").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.GET, "/apiCliente/regiones").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/apiCliente/cliente/imagen-perfil").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.POST, "/apiCliente/cliente").hasRole("ADMIN")
                .antMatchers("/apiCliente/cliente/**").hasRole("ADMIN")//PARA METODOS PUT Y DELETE
                */
                .anyRequest().authenticated()//cualquier otra peticion se requiere autenticacion
                .and().cors().configurationSource(corsConfigurationSource());
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource(){
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(Collections.singletonList("http://localhost:4200/"));
        config.addAllowedOriginPattern("*");
        config.setAllowedMethods(Arrays.asList("GET", "PUT", "POST", "DELETE", "OPTIONS"));
        config.setAllowCredentials(true);
        config.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter(){
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }

}
