import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'paginador-nav',
  templateUrl: './paginador.component.html',
  styleUrls: ['./paginador.component.css']
})
export class PaginadorComponent implements OnInit, OnChanges{

  //paginadorr variable en componente hijo
  @Input() paginadorr: any;
  paginas: number[];
  desde:number;
  hasta:number;

  constructor() { }

  ngOnChanges(cambios: SimpleChanges): void {
    let paginadorActualizado = cambios['paginadorr']
    if(paginadorActualizado.previousValue){
      this.initPaginador();
    }

  }

  ngOnInit(): void {
    this.initPaginador();
  }

  private initPaginador(): void{
    this.desde = Math.min(Math.max(1, this.paginadorr.number - 4), this.paginadorr.totalPages - 5);
    this.hasta = Math.max(Math.min(this.paginadorr.totalPages, this.paginadorr.number + 4), 6);
    if(this.paginadorr.totalPages > 5){
      this.paginas = new Array(this.hasta - this.desde - 1).fill(0).map((valor, indice) => indice + this.desde);
    }else{
      this.paginas = new Array(this.paginadorr.totalPages).fill(0).map((valor, indice) => indice + 1);
    }
  }

}
