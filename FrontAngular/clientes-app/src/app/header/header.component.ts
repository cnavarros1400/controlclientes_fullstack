import Swal from 'sweetalert2';
import { AuthService } from './../usuario/auth.service';
import { Component } from '@angular/core'
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent{
 titulo : string = "Logo navBar"

constructor(public authService: AuthService, private router: Router){  }

cerrarSesion(): void{
  let usu = this.authService.usuario.nombre + " " + this.authService.usuario.apellido;
  this.authService.cerrarSesion();
  Swal.fire("Cerrar sesión", `Se ha cerrado sesión para ${usu}`, "info")
  this.router.navigate(["/login"]);
}

}
