import { TokenInterceptor } from './usuario/interceptors/token.interceptor';
import { AuthInterceptor } from './usuario/interceptors/auth.interceptor';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ClienteService } from './cliente/cliente.service';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormularioComponent } from './cliente/formulario.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import localeES from '@angular/common/locales/es-MX'
import { registerLocaleData } from '@angular/common';
import { PaginadorComponent } from './paginador/paginador.component';
import {CalendarModule} from 'primeng/calendar';
import { DetalleComponent } from './cliente/detalle/detalle.component';
import { LoginComponent } from './usuario/login.component';
import { AuthGuard } from './usuario/guards/auth.guard';
import { RoleGuard } from './usuario/guards/role.guard';
import { DetalleFacturaComponent } from './factura/detalle-factura.component';
import { FacturaComponent } from './factura/factura.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';


registerLocaleData(localeES, "es");

const routes: Routes = [
  {path: '', redirectTo: './cliente', pathMatch: 'full'},
  {path: 'directiva', component: DirectivaComponent},
  {path: 'cliente', component: ClienteComponent},
  {path: 'cliente/pagina/:pagina', component: ClienteComponent},
  {path: 'cliente/formulario', component: FormularioComponent, canActivate: [AuthGuard, RoleGuard], data: {role: "ROLE_ADMIN"}},
  {path: 'cliente/formulario/:id', component: FormularioComponent, canActivate: [AuthGuard, RoleGuard], data: {role: "ROLE_ADMIN"}},
  {path: 'login', component: LoginComponent},
  {path: 'factura/:id', component: DetalleFacturaComponent, canActivate: [AuthGuard]},
  {path: 'factura/form/:clienteeId', component: FacturaComponent, canActivate: [AuthGuard, RoleGuard], data: {role: "ROLE_ADMIN"}}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DirectivaComponent,
    ClienteComponent,
    FormularioComponent,
    PaginadorComponent,
    DetalleComponent,
    LoginComponent,
    DetalleFacturaComponent,
    FacturaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    CalendarModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatAutocompleteModule,
    MatInputModule,
    MatFormFieldModule
  ],
  providers: [ClienteService, {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true}, {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
