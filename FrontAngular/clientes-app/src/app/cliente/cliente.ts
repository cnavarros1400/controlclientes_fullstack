import { Factura } from './../factura/models/factura';
import { Region } from "./Region";

export class Cliente {
  id: number;
  nombre: string;
  apellido: string;
  correo: string;
  fecha: string;
  imagen: string;
  region: Region;
  facturas: Factura[];
  //eslo mismo que:
  //facturas: Array<Factura> = [];
}
