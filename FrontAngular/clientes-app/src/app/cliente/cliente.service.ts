import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Cliente } from './cliente';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { formatDate, DatePipe } from '@angular/common';
import { Region } from './Region';


@Injectable({
  providedIn: 'root',
})
export class ClienteService {
  private urlEndPoint: string = 'http://localhost:8080/apiCliente/listado';

  constructor(private http: HttpClient, private router: Router) {}

  getClientes(pagina: number): Observable<any[]> {
    //return of(CLIENTES);
    //return this.http.get<Cliente[]>(this.urlEndPoint);
    //es lo mismo que:
    return this.http.get(this.urlEndPoint + '/pagina/' + pagina).pipe(
      tap((response: any) => {
        console.log('ClienteService: Tap 1');
        (response.content as Cliente[]).forEach((element) => {
          console.log(element.apellido);
        });
      }),
      map((response: any) => {
        (response.content as Cliente[]).map((cliente) => {
          cliente.apellido = cliente.apellido.toUpperCase();
          let dataPipe = new DatePipe('es');
          cliente.fecha = formatDate(cliente.fecha, 'EEEE dd, MMMM yyyy', 'es');
          return cliente;
        });
        return response;
      }),
      tap((response) => {
        console.log('ClienteService: Tap 2');
        (response.content as Cliente[]).forEach((element) => {
          console.log(element.apellido);
        });
      })
    );
  }

  registrarCliente(cliente: Cliente): Observable<Cliente> {
    //mapeando la respuesta de forma manual para usar observable de tipo Cliente
    return this.http
      .post('http://localhost:8080/apiCliente/cliente', cliente)
      .pipe(
        map((response: any) => response.Cliente as Cliente),
        catchError((err) => {
          if (err.status == 400) {
            return throwError(() => new Error(err));
          }
          if (err.error.mensaje) {
            console.error(err.error.mensaje);
          }
          return throwError(() => new Error(err));
        })
      );
  }

  buscarCliente(id: number): Observable<Cliente> {
    return this.http
      .get<Cliente>(`http://localhost:8080/apiCliente/cliente/${id}`)
      .pipe(
        catchError((e) => {
          if (e.status != 401 && e.mensaje) {
            this.router.navigate(['/cliente']);
            console.error(e.mensaje);
          }
          return throwError(() => new Error(e));
        })
      );
  }

  modificarCliente(cliente: Cliente): Observable<any> {
    //observable y metodo http de tipo any para leer el map recibido del servidor
    return this.http
      .put<any>(
        `http://localhost:8080/apiCliente/cliente/${cliente.id}`,
        cliente)
      .pipe(
        catchError((e) => {
          if (e.status == 400) {
            return throwError(() => e);
          }
          if (e.error.mensaje) {
            console.error(e.error.mensaje);
          }
          return throwError(() => new Error(e));
        })
      );
  }

  eliminarCliente(id: number): Observable<Cliente> {
    return this.http
      .delete<Cliente>(`http://localhost:8080/apiCliente/cliente/${id}`)
      .pipe(
        catchError((e) => {
          if (e.error.mensaje) {
            console.error(e.error.mensaje);
          }
          return throwError(() => new Error(e));
        })
      );
  }

  subirFoto(archivo: File, id: any): Observable<HttpEvent<{}>> {
    let formData: FormData = new FormData();
    formData.append('archivo', archivo);
    formData.append('id', id);

    const req = new HttpRequest(
      'POST',
      `http://localhost:8080/apiCliente/cliente/imagen-perfil/`,
      formData,
      {
        reportProgress: true
      });
    return this.http.request(req);
  }

  getRegiones(): Observable<Region[]> {
    return this.http
      .get<Region[]>(`http://localhost:8080/apiCliente/regiones`);
  }


}
