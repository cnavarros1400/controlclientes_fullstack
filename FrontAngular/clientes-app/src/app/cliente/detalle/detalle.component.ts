import { AuthService } from './../../usuario/auth.service';
import { ModalService } from './modal.service';
import Swal from 'sweetalert2';
import { Cliente } from './../cliente';
import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../cliente.service';
import { HttpEventType } from '@angular/common/http';
import { Input } from '@angular/core';
import { FacturaService } from 'src/app/factura/service/factura.service';
import { Factura } from 'src/app/factura/models/factura';

@Component({
  selector: 'detalle-cliente',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],
})
export class DetalleComponent implements OnInit {

  titulo:string = "Detalle de cliente"
  @Input() cliente: Cliente;
  progress: number = 0;
  imgSeleccionada: File;

  constructor(
    private clienteService: ClienteService,
    public modalService: ModalService,
    public authService: AuthService,
    private facturaService: FacturaService
  ) {}

  ngOnInit(): void {

  }

  subirFoto(){
    if(!this.imgSeleccionada){
      Swal.fire("Archivo vacío", "Favor de seleccionar un archivo en formato de imagen", "error");
    }else{
      this.clienteService.subirFoto(this.imgSeleccionada, this.cliente.id).subscribe(event => {
        //this.cliente = cliente;
        if(event.type === HttpEventType.UploadProgress){
          this.progress = Math.round((event.loaded/event.total)*100)
        }else if(event.type === HttpEventType.Response){
          let response: any = event.body;
          this.cliente = response.cliente as Cliente;
          this.modalService.notificarUpload.emit(this.cliente);//metodo getter sin parentesis
          Swal.fire("Carga correcta", response.mensaje, "success");
        }
      });
    }

  }

  seleccionarFoto(event){
    this.imgSeleccionada = event.target.files[0];
    this.progress = 0;
    console.log(this.imgSeleccionada)
    if(this.imgSeleccionada.type.indexOf("image") < 0){
      Swal.fire("Formato de archivo incorrecto", "Favor de seleccionar un archivo en formato de imagen", "error");
    }
  }

  cerrarModal(){
    this.modalService.cerrarModal();
    this.imgSeleccionada = null;
    this.progress = 0;
  }

  deleteFactura(factura: Factura): void{
    Swal.fire({
      title: 'Borrar factura.',
      text: `¿Desea borrar la factura ${factura.descripcion}? Esta accion es irreversible.`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar.',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.facturaService.deleteFactura(factura.id).subscribe(
          response => {
            this.cliente.facturas = this.cliente.facturas.filter(fact => fact !== factura)
            Swal.fire(
              'Factura eliminada',
              `El registro de la factura ${factura.descripcion}se ha eliminado exitosamente.`,
              'success'
            )
          })
      }
    })
  }

}
