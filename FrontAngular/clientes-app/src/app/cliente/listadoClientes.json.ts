import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
  {id: 1, nombre: "Svein", apellido: "Navarro", correo: "nava@mail.com", fecha: "01/01/2022"},
  {id: 2, nombre: "Luxanna", apellido: "Crawnguard", correo: "lux@lol.com", fecha: "02/02/2022"},
  {id: 3, nombre: "Katarina", apellido: "DuCoteau", correo: "kata@runaterra.com", fecha: "03/03/2022"}
];
