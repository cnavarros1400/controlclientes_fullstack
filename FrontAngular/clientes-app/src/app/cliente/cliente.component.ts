import { AuthService } from './../usuario/auth.service';
import { ModalService } from './detalle/modal.service';
import Swal from 'sweetalert2';
import { ClienteService } from './cliente.service';
import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {


  ppaginador: any;//variable padre
  clientes: Cliente[];
  clienteSeleccionado: Cliente;

  //dentro del constructor se hace la inyeccion de dependencia clienteService
  constructor(private clienteService: ClienteService,
              private activatedRouter: ActivatedRoute,
              public modalService: ModalService,
              public authService: AuthService) {}

  ngOnInit(): void {

    this.activatedRouter.paramMap.subscribe( params => {
      let pagina: number = +params.get('pagina');
      if(!pagina){
        pagina = 0;
      }
      this.clienteService.getClientes(pagina).pipe(
        tap((response: any) => {
          console.log("ClienteComponent: Tap 3");
          (response.content as Cliente[]).forEach(clt =>{
            console.log(clt.apellido)
          });
        })
      ).subscribe(
        response => {this.clientes = response.content as Cliente[];//puede ir tambien dentro del metodo tap y dejar subscribe vacio
        this.ppaginador = response;
      });
    });

    this.modalService.notificarUpload.subscribe(cliente =>{
      this.clientes = this.clientes.map(clienteOriginal => {
        if(cliente.id == clienteOriginal.id){
          clienteOriginal.imagen = cliente.imagen;
        }
        return clienteOriginal;
      });
    });

  }

  eliminarCliente(cliente: Cliente): void{
    Swal.fire({
      title: 'Confirmar borrado de información.',
      text: `¿Desea borrar el registro de cliente ${cliente.nombre} ${cliente.apellido}? Esta accion es irreversible.`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar.',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.clienteService.eliminarCliente(cliente.id).subscribe(
          response => {
            this.clientes = this.clientes.filter(clt => clt !== cliente)
            Swal.fire(
              'Registro eliminado',
              `El registro del cliente ${cliente.nombre} ${cliente.apellido} se ha eliminado exitosamente.`,
              'success'
            )
          })
      }
    })
  }

  abrirModal(cliente: Cliente){
    this.clienteSeleccionado = cliente;
    this.modalService.abrirmodal();
  }

}
