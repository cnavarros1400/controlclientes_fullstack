import { ClienteService } from './cliente.service';
import { Cliente } from './cliente';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { Region } from './Region';


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
})
export class FormularioComponent implements OnInit {
  public cliente: Cliente = new Cliente();
  public titulo: string = 'Registrar cliente';
  public errores: string[];
  public es: any;
  public regiones: Region[]

  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private activatedRouter: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarCliente();
    this.clienteService.getRegiones().subscribe(regiones => this.regiones = regiones);
    }

  registrarCliente(): void {
    this.clienteService.registrarCliente(this.cliente).subscribe({
      next: (response) => {
        this.router.navigate(['/cliente']);
        Swal.fire(
          'Cliente guardado',
          `Cliente ${response.nombre}` +
            ` ${response.apellido} registrado con éxito.`,
          'success'
        );
      },
      error: (err) => {
        this.errores = err.error.errores as string[];
        console.error('codigo de error desde servidor: ' + err.status);
        console.error(err.error.errores);
      },
    });
  }

  public cargarCliente(): void {
    this.activatedRouter.params.subscribe((params) => {
      let id = params['id'];
      if (id) {
        this.clienteService
          .buscarCliente(id)
          .subscribe((cliente) => (this.cliente = cliente));
      }
    });
  }

  public modificarCliente(): void {
    this.cliente.facturas = null;//evitar recursion con facturas al editar
    this.clienteService.modificarCliente(this.cliente).subscribe({
      next: (response) => {
        this.router.navigate(['/cliente']);
        Swal.fire(
          'Datos actualizados',
          `Cliente ${response.modificacion.nombre} ${response.modificacion.apellido} modificado exitosamente.`,
          'success'
        );
      },
      error: (err) => {
        this.errores = err.error.errores as string[];
        console.error('codigo de error desde servidor: ' + err.status);
        console.error(err.error.errores);
      },
    });
  }

  compararRegion(o1: Region, o2: Region): boolean{
    if(o1 === undefined && o2 === undefined){
      return true;
    }
    return o1 === null || o2 === null || o1 === undefined || o2 === undefined? false : o1.id === o2.id;
  }

}
