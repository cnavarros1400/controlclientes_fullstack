export class Usuario{
  id: number;
  username: string;
  pass: string;
  nombre: string;
  apellido: string;
  correo: string;
  roles: string[] = [];
}
