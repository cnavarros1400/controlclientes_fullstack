import { Usuario } from './usuario';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  titulo: string = "Iniciar sesión en sistema"
  usuario: Usuario;

  constructor(private authService: AuthService, private router: Router) {
    this.usuario = new Usuario();
   }

  ngOnInit(): void {

    if(this.authService.isAuthenticated()){
      Swal.fire("Sesión de usuario activa", `Ya existe una sesión en sistema para: ${this.authService.usuario.nombre} ${this.authService.usuario.apellido}`, "info");
      this.router.navigate(["/cliente"]);
    }
  }

  login(): void{
    console.log(this.usuario);
    if (this.usuario.username == null || this.usuario.pass == null){
      Swal.fire("Error de acceso", "Usuario o contraseña vacío", "error");
      return;
    }
    this.authService.login(this.usuario).subscribe(response => {
      console.log("Respuesta de servidor. " + response);
      this.authService.guardarUsuario(response.access_token);
      this.authService.guardarToken(response.access_token);
      let usuario = this.authService.usuario;
      this.router.navigate(["/cliente"]);
      Swal.fire("Acceso correcto", `Acceso concedido para: ${usuario.nombre} ${response.apellido} \t Tipo de acceso. ${usuario.roles}` , "success");
    },err=>{
      if(err.status == 400){
        Swal.fire("Credenciales incorrectas", "Usuario o contraseña incorrectos.", "error");//recomendable no especificar cual campo es incorrecto, por seguridad
      }
    });
  }


}
