import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private authService: AuthService, private ruta: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.authService.isAuthenticated()) {
        this.ruta.navigate(["/login"]);
        return false;
      }
      let rolValidar = route.data["role"] as string;
      console.log(rolValidar);
      if (this.authService.hasRole(rolValidar)) {
        return true;
      }
      Swal.fire("Acceso denegado", `Usuario ${this.authService.usuario.nombre} ${this.authService.usuario.apellido}: sin permisos suficientes`, "warning");
      return false;
  }

}
