import { AuthService } from './../auth.service';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private ruta: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.authService.isAuthenticated()) {
      if (this.isTokenExpired()) {
        this.authService.cerrarSesion();
        this.ruta.navigate(['/login']);
        return false;
      }
      return true;
    }
    this.ruta.navigate(['/login']);
    return false;
  }

  isTokenExpired(): boolean{
    let token = this.authService.token;
    let payload = this.authService.obtenerPayload(token);
    let now = new Date().getTime()/1000;
    if(payload.exp < now){
        return true;
    }
    return false;
  }

}
