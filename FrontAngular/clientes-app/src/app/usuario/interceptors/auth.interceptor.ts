import { AuthService } from './../auth.service';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError(e => {
        if (e.status == 401) {
          //401=no auttenticado - 403=no autorizado
          if(this.authService.isAuthenticated()){
            this.authService.cerrarSesion();
          }
          this.router.navigate(['/login']);
        }
        if (e.status == 403) {
          //401=no auttenticado - 403=no autorizado
          Swal.fire("Acceso denegado", `Usuario ${this.authService.usuario.nombre} ${this.authService.usuario.apellido}: sin permisos suficientes`, "warning");
          this.router.navigate(['/cliente']);
        }
        return throwError(() => new Error(e));
      })
    );
  }
}
