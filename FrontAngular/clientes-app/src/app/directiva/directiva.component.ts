import { Component} from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.css']
})
export class DirectivaComponent{

  habilitar: boolean = true;
  listaCursos: string[] = ['typescript', "javascript", 'php', "java"]

  constructor() { }

  setHabilitar(): void{
    this.habilitar = (this.habilitar==true)? false:true;
  }

  setEtiquetaBtn(): string{
    if(this.habilitar == true){
      return "Ocultar cursos"
    }else{
      return "Mostrar cursos"
    }
  }

}
