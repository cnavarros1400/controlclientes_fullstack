import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Factura } from '../models/factura';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private urlEndPoint = "http://localhost:8080/apiFactura"

  constructor(private httpClient: HttpClient) { }

  getFactura(id: number): Observable<Factura>{
    return this.httpClient.get<Factura>(`${this.urlEndPoint}/factura/${id}`);
  }

  deleteFactura(id: number): Observable<void>{
    return this.httpClient.delete<void>(`${this.urlEndPoint}/factura/${id}`);
  }

  buscarProducto(termino: string): Observable<Producto[]>{
    return this.httpClient.get<Producto[]>(`${this.urlEndPoint}/factura/buscar-producto/${termino}`);
  }

  crearFactura(factura: Factura): Observable<Factura>{
    return this.httpClient.post<Factura>(`${this.urlEndPoint}/factura`, factura);
  }

}
