import { Cliente } from './../../cliente/cliente';
import { ItemFactura } from "./item-factura";

export class Factura {
  id: number;
  descripcion: string;
  observacion: string;
  fecha: string;
  cliente: Cliente;
  items: Array<ItemFactura> = [];
  total: number;

  calcularTotal(): number{
    this.total = 0;
    this.items.forEach((item: ItemFactura) => {
      this.total += item.calcularImporte();
    });
    return this.total;
  }

}
