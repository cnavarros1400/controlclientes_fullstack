import Swal from 'sweetalert2';
import { ItemFactura } from './models/item-factura';
import { Factura } from './models/factura';
import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/cliente/cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map, mergeMap, Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { FacturaService } from './service/factura.service';
import { Producto } from './models/producto';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.css'],
})
export class FacturaComponent implements OnInit {
  titulo: string = 'Nueva factura';
  factura: Factura = new Factura();
  //Autocomplete Angular material
  //productos: string[] = ["p1", "p2", "p3"];
  autocompleteControl = new FormControl();
  productosFiltrados: Observable<Producto[]>;

  constructor(
    private clienteService: ClienteService,
    private activatedRoute: ActivatedRoute,
    private facturaService: FacturaService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      let idCliente = +params.get('clienteeId');
      this.clienteService
        .buscarCliente(idCliente)
        .subscribe((cliente) => (this.factura.cliente = cliente));
    });
    //autocomplete angular material
    this.productosFiltrados = this.autocompleteControl.valueChanges.pipe(
      map((value) => (typeof value === 'string' ? value : value.nombre)),
      mergeMap((value) => (value ? this._filter(value) : []))
    );
  }

  private _filter(value: string): Observable<Producto[]> {
    const filterValue = value.toLowerCase();
    return this.facturaService.buscarProducto(filterValue);
  }

  mostrarNombre(producto?: Producto): string | undefined {
    return producto ? producto.nombre : undefined;
  }

  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
    let producto = event.option.value as Producto;
    console.log('Producto seleccionado: ' + producto.nombre);
    if (this.productoAgregado(producto.id)) {
      this.incrementarCantidad(producto.id);
    } else {
      let nuevoItem = new ItemFactura();
      nuevoItem.producto = producto;
      this.factura.items.push(nuevoItem);
    }
    this.autocompleteControl.setValue('');
    event.option.focus();
    event.option.deselect;
  }

  actualizarCantidad(idProd: number, event: any): void {
    let nuevaCantidad: number = event.target.value as number;
    if (nuevaCantidad == 0) {
      return this.quitarItem(idProd);
    }
    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (idProd === item.producto.id) {
        item.cantidad = nuevaCantidad;
      }
      return item;
    });
  }

  productoAgregado(idProd: number): boolean {
    let existe = false;
    this.factura.items.forEach((item: ItemFactura) => {
      if (idProd === item.producto.id) {
        existe = true;
      }
    });
    return existe;
  }

  incrementarCantidad(idProd: number): void {
    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (idProd === item.producto.id) {
        ++item.cantidad;
      }
      return item;
    });
  }

  quitarItem(idProd: number): void {
    this.factura.items = this.factura.items.filter((item: ItemFactura) => {
      return idProd !== item.producto.id; //quitar llaves, quitar return, con llaves se indica salida de la funcion con return
    });
  }

  crearFactura(facturaForm): void {
    if (this.factura.items.length == 0) {
      this.autocompleteControl.setErrors({'invalid': true});
    }
    if (facturaForm.form.valid || this.factura.items.length > 0) {
      console.log('Factura a registrar: ' + this.factura);
      this.facturaService.crearFactura(this.factura).subscribe((factura) => {
        Swal.fire(this.titulo, 'Factura creada exitosamente: ' + factura.descripcion, 'success');
        this.router.navigate(['/factura', factura.id]);
      });
    }
  }
}
