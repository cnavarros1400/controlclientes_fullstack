import { Factura } from './models/factura';
import { Component, OnInit } from '@angular/core';
import { FacturaService } from './service/factura.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.css']
})
export class DetalleFacturaComponent implements OnInit {

  factura: Factura;
  titulo: string = "Factura";


  constructor(private facturaService: FacturaService, private activateddRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activateddRoute.paramMap.subscribe(params => {
      let id = +params.get("id");//simbolo + para convertir a number
      this.facturaService.getFactura(id).subscribe(factura => this.factura = factura);
    });
  }

}
