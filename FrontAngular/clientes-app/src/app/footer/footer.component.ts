import { Component} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent{

  //objetos any son objetos genericos que no se asocian a ninguna clase
  public autor : any = {nombre: "Svein", apellido: "Navarro"};

}
